/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.codeshaped.timerwithsound;

import java.io.IOException;
import java.util.TimeZone;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;

import java.lang.Runnable;

import android.os.Debug;
import android.os.Handler;

import android.provider.Settings;
import android.util.Log;

public class TimerWithSound extends CordovaPlugin {
    public static final String TAG = "TimerWithSound";
    public ToneGenerator toneGen1;
    private final Handler timeoutHandler = new Handler();
    private final boolean running[] = {false};

    private AssetFileDescriptor afdShort;
    private MediaPlayer beepSoundShort;
    private AssetFileDescriptor afdLong;
    private MediaPlayer beepSoundLong;

    /**
     * Constructor.
     */
    public TimerWithSound() {
        toneGen1= new ToneGenerator(AudioManager.STREAM_MUSIC, 100); 
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        try {
            afdShort = cordova.getActivity().getApplicationContext().getAssets().openFd("www/beepShort.mp3");
            beepSoundShort = new MediaPlayer();
            beepSoundShort.setDataSource(afdShort.getFileDescriptor(),afdShort.getStartOffset(),afdShort.getLength());
            beepSoundShort.prepare();
            afdLong = cordova.getActivity().getApplicationContext().getAssets().openFd("www/beepLong.mp3");
            beepSoundLong = new MediaPlayer();
            beepSoundLong.setDataSource(afdLong.getFileDescriptor(),afdLong.getStartOffset(),afdLong.getLength());
            beepSoundLong.prepare();
            Log.e("timer with sound plugin", "File found!");
        }catch (IOException e) {
            Log.e("timer with sound plugin", "No file found!");
        }
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if("startTimer".equals(action)) {
            if(!running[0]) {
                running[0] = true;
                int delay = args.getInt(0);
                int iterations = args.getInt(1);
                if(delay < iterations) {
                    iterations = delay;
                    delay = 0;
                }
                beepInSeconds(Math.max(0, delay - iterations + 1), iterations);
            }
        } else {
            running[0] = false;
            timeoutHandler.removeCallbacksAndMessages(null);
        }
        return true;
    }

    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------
    private void beepInSeconds(int seconds, final int count) {
        if(running[0]) {
            timeoutHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (count > 0) {
                        beepSoundShort.start();
                        beepInSeconds(1, count - 1);
                    } else {
                        beepSoundLong.start();
                        running[0] = false;
                    }
                }
            }, seconds * 1000);
        }
    }

}
