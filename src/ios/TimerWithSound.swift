
// import this
import AVFoundation

@objc(TimerWithSound) class TimerWithSound : CDVPlugin {
  
  var player: AVAudioPlayer?
  var continuousPlayer: AVAudioPlayer?
  var isRunning: Bool = false
  var timer: Timer?
  var beepUrl: String?
  var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid


  override func pluginInitialize() {
    super.pluginInitialize()
    print("INIT TimerWithSound Plugin")

    do {
      try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
      do {
        try AVAudioSession.sharedInstance().setActive(true)
        print("AV Audio Session worked")
      }
      catch {
        print("ERROR: setActive not working")
      }

    } catch {
      print("ERROR: AVAudioSession init not working")
    }
  }

  func registerBackgroundTask() {
    backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
      self?.endBackgroundTask()
    }
    assert(backgroundTask != UIBackgroundTaskInvalid)
  }

  func endBackgroundTask() {
    print("Background task ended.")
    UIApplication.shared.endBackgroundTask(backgroundTask)
    backgroundTask = UIBackgroundTaskInvalid
  }


  func setTimeout(_ delay: TimeInterval, block:@escaping ()-> Void) -> Timer {
    return Timer.scheduledTimer(timeInterval: delay, target: BlockOperation(block: block), selector: #selector(Operation.main), userInfo: nil, repeats: false)
  }

  func playBeep(_ audioName: String) {

    guard let beepUrl = Bundle.main.url(forResource: audioName, withExtension: "mp3", subdirectory: "www") else {
      print("beepUrl not found")
      return
    }

    do {
      player = try AVAudioPlayer(contentsOf: beepUrl, fileTypeHint: AVFileTypeMPEGLayer3)
      player?.prepareToPlay()
      player?.play()

    } catch {
      print("ERROR: AVAudioPlayer init not working")
    }
  }

  func playShortBeep() {
    self.playBeep("beepShort")
  }

  func playLongBeep() {
    self.playBeep("beepLong")
  }

  func handleBeeps(_ currentDelay: TimeInterval, count: Int) {
    self.timer = self.setTimeout(currentDelay, block: {() -> Void in

      print("HANDLE BEEPS")
        print(currentDelay)
        
      switch UIApplication.shared.applicationState {
      case .active:
        print("App is foreground.")
      case .background:
        print("App is background.")
        print("Background time remaining = \(UIApplication.shared.backgroundTimeRemaining) seconds")
      case .inactive:
        break
      }


      let newCount = count - 1
      print("count")
      print(count)
      if (count > 0) {
        self.playShortBeep()
        self.handleBeeps(1, count: newCount)
      }
      else {
        self.playLongBeep()
        self.isRunning = false
        self.continuousPlayer?.stop()
        //self.endBackgroundTask()
      }
    })
  }

  @objc(startTimer:)
  func startTimer(_ command: CDVInvokedUrlCommand) {

    if (command.arguments.count < 2) {
      print("Error: missing param", command.arguments)
      return
    }

    guard let delay: Int = command.arguments[0] as? Int else {
      print("Error: delay param not set")
      return
    }

    guard let iterations: Int = command.arguments[1] as? Int else {
      print("Error: iterations param not set")
      return
    }

    print("running")
    print(self.isRunning)

    if (self.isRunning) {
      print("already running")
      return
    }

    let currentDelay: TimeInterval = TimeInterval(max(delay - iterations, 0))
    self.isRunning = true

    print("START", currentDelay, min(iterations, delay))
    
    guard let silentUrl = Bundle.main.url(forResource: "silent", withExtension: "mp3", subdirectory: "www") else {
        print("beepUrl not found")
        return
    }
    
    do {
        continuousPlayer = try AVAudioPlayer(contentsOf: silentUrl, fileTypeHint: AVFileTypeMPEGLayer3)
        continuousPlayer?.prepareToPlay()
        continuousPlayer?.volume = 1
        continuousPlayer?.play()
        print("Play Silent MP3")
        
    } catch {
        print("ERROR: AVAudioPlayer init not working")
    }

    self.handleBeeps(currentDelay, count: min(iterations, delay))
    // self.registerBackgroundTask()
  }

  @objc(endTimer:)
  func endTimer(_ command: CDVInvokedUrlCommand) {
    print("endTimer")
    if (self.timer != nil) {
      self.isRunning = false
      self.timer!.invalidate()
      if backgroundTask != UIBackgroundTaskInvalid {
        endBackgroundTask()
      }
    }
  }
}
